package com.mh.repository.impl;

import com.mh.repository.AbsMysqlRepository;
import com.mh.tables.pojos.Topic;
import com.mh.tables.records.TopicRecord;
import org.jooq.impl.TableImpl;
import org.springframework.stereotype.Repository;

import static com.mh.tables.Topic.TOPIC;

@Repository
public class TopicRepositoryImpl extends AbsMysqlRepository<Topic, TopicRecord> {
    @Override
    public TableImpl<TopicRecord> getTable() {
        return TOPIC;
    }
}
