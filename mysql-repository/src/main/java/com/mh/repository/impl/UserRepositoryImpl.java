package com.mh.repository.impl;

import com.mh.repository.AbsMysqlRepository;
import com.mh.tables.pojos.User;
import com.mh.tables.records.UserRecord;
import org.jooq.impl.TableImpl;
import org.springframework.stereotype.Repository;

import static com.mh.tables.User.USER;

@Repository
public class UserRepositoryImpl extends AbsMysqlRepository<User, UserRecord> {
    @Override
    public TableImpl<UserRecord> getTable() {
        return USER;
    }
}
