package com.mh.data.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TopicResponse {
    private String id;
    private String content;
    private UserResponse user;
}
