package com.mh.data.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TopicRequest {
    private String id;
    private String content;
    private String userId;
}
