package com.mh.data.mapper;

import com.mh.data.request.UserRequest;
import com.mh.data.response.UserResponse;
import com.mh.tables.pojos.User;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class UserMapper {
    @Named("toUserResponse")
    public abstract UserResponse toUserResponse(User user);

    @IterableMapping(qualifiedByName = "toUserResponse")
    public abstract List<UserResponse> toUserResponses(List<User> userList);

    public abstract User toUser(UserRequest userRequest);
}
