package com.mh.data.mapper;

import com.mh.data.request.TopicRequest;
import com.mh.data.response.TopicResponse;
import com.mh.tables.pojos.Topic;
import com.mh.tables.pojos.User;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class TopicMapper {

    @Autowired private UserMapper userMapper;

    @Named("toTopicResponse")
    public abstract TopicResponse toTopicResponse(Topic topic);

    @IterableMapping(qualifiedByName = "toTopicResponse")
    public abstract List<TopicResponse> toTopicResponses(List<Topic> topicList);

    public TopicResponse toTopicResponse(Topic topic, User user) {
        TopicResponse topicResponse = toTopicResponse(topic);
        topicResponse.setUser(userMapper.toUserResponse(user));
        return topicResponse;
    }

    public abstract Topic toTopic(TopicRequest topicRequest);
}
