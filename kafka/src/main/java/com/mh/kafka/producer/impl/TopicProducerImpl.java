package com.mh.kafka.producer.impl;

import com.mh.kafka.data.TopicMessage;
import com.mh.kafka.producer.BaseProducer;
import org.springframework.stereotype.Component;

@Component
public class TopicProducerImpl extends BaseProducer<TopicMessage> {
}
