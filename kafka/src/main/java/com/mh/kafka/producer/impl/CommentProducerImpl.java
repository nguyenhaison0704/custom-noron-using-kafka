package com.mh.kafka.producer.impl;

import com.mh.kafka.data.CommentMessage;
import com.mh.kafka.producer.BaseProducer;
import org.springframework.stereotype.Component;

@Component
public class CommentProducerImpl extends BaseProducer<CommentMessage> {
}
