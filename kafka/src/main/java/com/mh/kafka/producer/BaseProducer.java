package com.mh.kafka.producer;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Data
public abstract class BaseProducer<T> {
    @Autowired private KafkaTemplate<String, Object> kafkaTemplate;

    private String topicName = "noron";

    public int sendMessage(T t) {
        ListenableFuture<SendResult<String, Object>> future =
            kafkaTemplate.send(topicName, t);
        future.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onSuccess(SendResult<String, Object> result) {
                System.out.println("Sent message=[" + t.toString() +
                        "] with offset=[" + result.getRecordMetadata().offset() + "]");
            }
            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Unable to send message=["
                        + t + "] due to : " + ex.getMessage());
            }
        });
        return 1;
    }
}
