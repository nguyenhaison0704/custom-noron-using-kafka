package com.mh.kafka.producer.impl;

import com.mh.kafka.data.UserMessage;
import com.mh.kafka.producer.BaseProducer;
import org.springframework.stereotype.Component;

@Component
public class UserProducerImpl extends BaseProducer<UserMessage> {
}
