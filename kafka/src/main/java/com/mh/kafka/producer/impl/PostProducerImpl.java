package com.mh.kafka.producer.impl;

import com.mh.kafka.data.PostMessage;
import com.mh.kafka.producer.BaseProducer;
import org.springframework.stereotype.Component;

@Component
public class PostProducerImpl extends BaseProducer<PostMessage> {
}
