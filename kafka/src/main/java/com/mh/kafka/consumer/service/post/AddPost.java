package com.mh.kafka.consumer.service.post;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.elasticsearch.service.PostESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.BaseMessage;
import com.mh.kafka.data.PostMessage;
import com.mh.repository.impl.PostRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.POST_ADD;

@Identity(POST_ADD)
public class AddPost extends AbsMethodHandle<PostMessage, PostRepositoryImpl, PostESService> {
    @Override
    public Integer executeMethod(List<PostMessage> postMessages) {
        return insertPosts(postMessages);
    }

    public Integer insertPosts(List<PostMessage> postMessageList) {
        if(postMessageList == null) return 0;
        getRepository().createMany(postMessageList.stream().map(BaseMessage::getT).collect(Collectors.toList()));
        return 1;
    }
}
