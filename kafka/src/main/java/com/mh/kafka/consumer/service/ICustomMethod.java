package com.mh.kafka.consumer.service;

import java.util.List;

public interface ICustomMethod<P> {
    Integer handle(List<P> tList);
}
