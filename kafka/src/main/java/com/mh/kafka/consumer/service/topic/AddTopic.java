package com.mh.kafka.consumer.service.topic;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.elasticsearch.service.PostESService;
import com.mh.elasticsearch.service.TopicESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.BaseMessage;
import com.mh.kafka.data.TopicMessage;
import com.mh.repository.impl.TopicRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.TOPIC_ADD;

@Identity(TOPIC_ADD)
public class AddTopic extends AbsMethodHandle<TopicMessage, TopicRepositoryImpl, TopicESService> {
    @Override
    public Integer executeMethod(List<TopicMessage> topicMessages) {
        return insertTopics(topicMessages);
    }

    public Integer insertTopics(List<TopicMessage> topicMessageList) {
        if(topicMessageList == null) return 0;
        getRepository().createMany(topicMessageList.stream().map(BaseMessage::getT).collect(Collectors.toList()));
        return 1;
    }
}
