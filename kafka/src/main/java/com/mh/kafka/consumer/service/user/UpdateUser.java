package com.mh.kafka.consumer.service.user;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.UserESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.BaseMessage;
import com.mh.kafka.data.UserMessage;
import com.mh.repository.impl.UserRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.USER_UPDATE;

@Identity(USER_UPDATE)
public class UpdateUser extends AbsMethodHandle<UserMessage, UserRepositoryImpl, UserESService> {
    @Override
    public Integer executeMethod(List<UserMessage> userMessages) {
        return updateUsers(userMessages) + updateES(userMessages);
    }

    public Integer updateUsers(List<UserMessage> userMessages) {
        if (userMessages == null) return 0;
        getRepository().updateMany(userMessages.stream().map(BaseMessage::getT).collect(Collectors.toList()));
        return 1;
    }

    public Integer updateES(List<UserMessage> userMessages) {
        if (userMessages == null) return 0;
        getEsService().updateMany(userMessages.stream().map(BaseMessage::getT).collect(Collectors.toList()));
        return 1;
    }

}