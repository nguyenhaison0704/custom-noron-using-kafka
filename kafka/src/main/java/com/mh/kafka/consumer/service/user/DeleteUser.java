package com.mh.kafka.consumer.service.user;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.UserESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.UserMessage;
import com.mh.repository.impl.UserRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.USER_DEL;

@Identity(USER_DEL)
public class DeleteUser extends AbsMethodHandle<UserMessage, UserRepositoryImpl, UserESService> {
    @Override
    public Integer executeMethod(List<UserMessage> userMessages) {
        return deleteUser(userMessages.parallelStream()
                .map(UserMessage::getId)
                .collect(Collectors.toList()))
                + deleteES(userMessages.parallelStream()
                .map(UserMessage::getId)
                .collect(Collectors.toList()));
    }

    public Integer deleteUser(List<String> idList) {
        if(idList == null) return 0;
        return getRepository().deleteMany(idList);
    }

    public Integer deleteES(List<String> ids) {
        if(ids == null) return 0;
        return getEsService().deleteMany(ids);
    }
}
