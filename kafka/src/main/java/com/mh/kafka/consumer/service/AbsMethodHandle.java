package com.mh.kafka.consumer.service;

import com.mh.elasticsearch.service.AbsESService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Data
public abstract class AbsMethodHandle<P, R, E extends AbsESService> implements ICustomMethod<P> {
    @Autowired private R repository;

    @Autowired private E esService;

    public abstract Integer executeMethod(List<P> pList);
    @Override
    public Integer handle(List<P> pList) {
        return this.executeMethod(pList);
    }
}
