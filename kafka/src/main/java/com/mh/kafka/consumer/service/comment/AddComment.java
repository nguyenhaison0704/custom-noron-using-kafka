package com.mh.kafka.consumer.service.comment;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.BaseMessage;
import com.mh.kafka.data.CommentMessage;
import com.mh.repository.impl.CommentRepositoryImpl;
import com.mh.repository.impl.PostRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.COMMENT_ADD;

@Identity(COMMENT_ADD)
public class AddComment extends AbsMethodHandle<CommentMessage, CommentRepositoryImpl, CommentESService> {

    @Autowired private PostRepositoryImpl postRepository;
    public Integer insertComments(List<CommentMessage> commentMessageList) {
        if(commentMessageList == null) return 0;
        postRepository.incNumCommentMultiPost(commentMessageList.stream().map(BaseMessage::getId).collect(Collectors.toList()));
        getRepository().createMany(commentMessageList.stream().map(BaseMessage::getT).collect(Collectors.toList()));
        return 1;
    }

    @Override
    public Integer executeMethod(List<CommentMessage> commentMessages) {
        return insertComments(commentMessages);
    }
}
