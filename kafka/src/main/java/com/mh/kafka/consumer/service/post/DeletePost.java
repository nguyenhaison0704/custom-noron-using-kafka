package com.mh.kafka.consumer.service.post;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.elasticsearch.service.PostESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.PostMessage;
import com.mh.repository.impl.PostRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.POST_DEL;

@Identity(POST_DEL)
public class DeletePost extends AbsMethodHandle<PostMessage, PostRepositoryImpl, PostESService> {
    @Override
    public Integer executeMethod(List<PostMessage> postMessageList) {
        return deletePost(postMessageList.parallelStream()
                .map(PostMessage::getId)
                .collect(Collectors.toList()));
    }

    public Integer deletePost(List<String> idList) {
        if(idList == null) return 0;
        return getRepository().deleteMany(idList);
    }
}
