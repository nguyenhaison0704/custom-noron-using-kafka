package com.mh.kafka.consumer;

import com.mh.kafka.config.factory.HandleMethodFactory;
import com.mh.kafka.data.BaseMessage;
import com.mh.repository.impl.CommentRepositoryImpl;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Component
public class BaseConsumer {

    @Autowired private CommentRepositoryImpl commentRepository;

    @Autowired private HandleMethodFactory handleMethodFactory;

    @KafkaListener(
            topics = "noron",
            containerFactory = "concurrentKafkaListenerContainerFactory")
    public void kafkaListener(List<BaseMessage> messageList) {

        Map<String, List<BaseMessage>> messageMap = messageList
                .stream()
                .collect(Collectors.groupingBy(BaseMessage::getMethod));

        messageMap.entrySet().forEach((entry -> {
            handleMethodFactory.getMETHOD_HANDLE_MAP().getOrDefault(entry.getKey(), null).parallelStream()
                    .forEach(iCustomMethod -> {
                        iCustomMethod.handle(entry.getValue());
                    });
        }));

        System.out.println("Received Message in group noron: " + messageList.toString());
    }
}
