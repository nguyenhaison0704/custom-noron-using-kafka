package com.mh.kafka.consumer.service.topic;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.elasticsearch.service.PostESService;
import com.mh.elasticsearch.service.TopicESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.TopicMessage;
import com.mh.repository.impl.TopicRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.TOPIC_DEL;

@Identity(TOPIC_DEL)
public class DeleteTopic extends AbsMethodHandle<TopicMessage, TopicRepositoryImpl, TopicESService> {
    @Override
    public Integer executeMethod(List<TopicMessage> topicMessages) {
        return deleteTopic(topicMessages.parallelStream()
                .map(TopicMessage::getId)
                .collect(Collectors.toList()));
    }

    public Integer deleteTopic(List<String> idList) {
        if(idList == null) return 0;
        return getRepository().deleteMany(idList);
    }
}
