package com.mh.kafka.consumer.service.comment;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.BaseMessage;
import com.mh.kafka.data.CommentMessage;
import com.mh.repository.impl.CommentRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.COMMENT_UPDATE;

@Identity(COMMENT_UPDATE)
public class UpdateComment extends AbsMethodHandle<CommentMessage, CommentRepositoryImpl, CommentESService> {

    public Integer updateComments(List<CommentMessage> commentMessageList) {
        if(commentMessageList == null) return 0;
        getRepository().updateMany(commentMessageList.stream().map(BaseMessage::getT).collect(Collectors.toList()));
        return 1;
    }

    @Override
    public Integer executeMethod(List<CommentMessage> commentMessages) {
        return updateComments(commentMessages);
    }
}
