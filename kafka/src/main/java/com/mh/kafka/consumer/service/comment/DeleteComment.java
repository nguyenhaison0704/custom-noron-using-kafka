package com.mh.kafka.consumer.service.comment;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.CommentMessage;
import com.mh.repository.impl.CommentRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.COMMENT_DEL;

@Identity(COMMENT_DEL)
public class DeleteComment extends AbsMethodHandle<CommentMessage, CommentRepositoryImpl, CommentESService> {

    public Integer deleteComment(List<String> idList) {
        if(idList == null) return 0;
        return getRepository().deleteMany(idList);
    }

    @Override
    public Integer executeMethod(List<CommentMessage> commentMessages) {
        return deleteComment(commentMessages.parallelStream()
                .map(CommentMessage::getId)
                .collect(Collectors.toList()));
    }

}
