package com.mh.kafka.consumer.service.topic;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.elasticsearch.service.PostESService;
import com.mh.elasticsearch.service.TopicESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.BaseMessage;
import com.mh.kafka.data.TopicMessage;
import com.mh.repository.impl.TopicRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.TOPIC_UPDATE;

@Identity(TOPIC_UPDATE)
public class UpdateTopic extends AbsMethodHandle<TopicMessage, TopicRepositoryImpl, TopicESService> {
    @Override
    public Integer executeMethod(List<TopicMessage> topicMessages) {
        return updateTopics(topicMessages);
    }

    public Integer updateTopics(List<TopicMessage> topicMessageList) {
        if(topicMessageList == null) return 0;
        getRepository().updateMany(topicMessageList.stream().map(BaseMessage::getT).collect(Collectors.toList()));
        return 1;
    }
}
