package com.mh.kafka.consumer.service.post;

import com.mh.annotation.Identity;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.elasticsearch.service.PostESService;
import com.mh.kafka.consumer.service.AbsMethodHandle;
import com.mh.kafka.data.PostMessage;
import com.mh.repository.impl.PostRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.mh.kafka.config.identity.IdentityToMethodMap.POST_INC_COMMENT;

@Identity(POST_INC_COMMENT)
public class IncreaseNumCommentPost extends AbsMethodHandle<PostMessage, PostRepositoryImpl, PostESService> {
    @Override
    public Integer executeMethod(List<PostMessage> postMessageList) {
        return incNumComment(postMessageList);
    }

    public Integer incNumComment(List<PostMessage> postMessageList) {
        if(postMessageList == null) return 0;
        getRepository().incNumCommentMultiPost(postMessageList.stream().map(PostMessage::getId).collect(Collectors.toList()));
        return 1;
    }
}
