package com.mh.kafka.config.identity;

public class IdentityToMethodMap {
    public static final String COMMENT_ADD = "comment:add";
    public static final String COMMENT_UPDATE = "comment:update";
    public static final String COMMENT_DEL = "comment:delete";

    public static final String POST_ADD = "post:add";
    public static final String POST_UPDATE = "post:update";
    public static final String POST_DEL = "post:delete";
    public static final String POST_INC_VIEW = "post:inc-view";
    public static final String POST_INC_COMMENT = "post:inc-comment";
    public static final String POST_DEC_COMMENT = "post:dec-comment";

    public static final String TOPIC_ADD = "topic:add";
    public static final String TOPIC_UPDATE = "topic:update";
    public static final String TOPIC_DEL = "topic:delete";

    public static final String USER_ADD = "user:add";
    public static final String USER_UPDATE = "user:update";
    public static final String USER_DEL = "user:delete";
}
