package com.mh.kafka.config.factory;

import com.mh.annotation.Identity;
import com.mh.kafka.consumer.service.ICustomMethod;
import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

@Data
@Component
public class HandleMethodFactory {
    private Map<String, List<ICustomMethod>> METHOD_HANDLE_MAP;

    public HandleMethodFactory(List<ICustomMethod> methodHandleMap) {
        METHOD_HANDLE_MAP = methodHandleMap.stream()
                .filter(handle -> handle.getClass().isAnnotationPresent(Identity.class))
                .flatMap(handle -> {
                    return Arrays.stream(handle.getClass().getAnnotation(Identity.class).value())
                            .map(value -> {
                                return Pair.of(value, handle);
                            });
                })
                .collect(groupingBy(Pair::getLeft, mapping(Pair::getRight, toList())));
    }
}
