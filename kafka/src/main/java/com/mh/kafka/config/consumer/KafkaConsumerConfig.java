package com.mh.kafka.config.consumer;

import lombok.Data;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Data
@Component
@EnableKafka
@Configuration
@ConfigurationProperties("spring.kafka.consumer")
public class KafkaConsumerConfig {
    private String bootstrapAddress;
    private String groupId;

//    private TypeReference<T> typeReference;

    public ConsumerFactory<String, Object> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(this.consumerProperties(),
                new StringDeserializer(),
                new JsonDeserializer<>(Object.class));
    };

    public Map<String, Object> consumerProperties() {
        Map<String, Object> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapAddress);
        props.put(
                ConsumerConfig.GROUP_ID_CONFIG,
                groupId);
        props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        props.put(
                ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG,
                1 * 1000);
        return props;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Object>
    concurrentKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, Object> factory
                = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(this.consumerFactory());
        return factory;
    }
}
