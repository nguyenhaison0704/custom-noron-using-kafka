package com.mh.kafka.data;

import com.mh.tables.pojos.Topic;

public class TopicMessage extends BaseMessage<Topic> {
    @Override
    public String getType() {
        return "topic";
    }
}
