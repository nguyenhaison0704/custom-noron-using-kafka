package com.mh.kafka.data;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public abstract class BaseMessage<T> {
    private T t;
    private String id;
    private String method;

    public abstract String getType();

    public BaseMessage(T t, String method) {
        this.t = t;
        this.method = method;
    }

    public BaseMessage() {
    }
}
