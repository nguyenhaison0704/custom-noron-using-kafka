package com.mh.kafka.data;

import com.mh.data.mongo.model.Comment;

public class CommentMessage extends BaseMessage<Comment> {

    @Override
    public String getType() {
        return "comment";
    }
}
