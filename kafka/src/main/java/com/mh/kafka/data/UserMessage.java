package com.mh.kafka.data;

import com.mh.tables.pojos.User;

public class UserMessage extends BaseMessage<User> {
    @Override
    public String getType() {
        return "user";
    }
}
