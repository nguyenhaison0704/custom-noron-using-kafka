package com.mh.kafka.data;

import com.mh.data.mongo.model.Post;

public class PostMessage extends BaseMessage<Post>{
    @Override
    public String getType() {
        return "post";
    }
}
