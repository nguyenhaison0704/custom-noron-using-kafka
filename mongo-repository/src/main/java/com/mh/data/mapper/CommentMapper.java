package com.mh.data.mapper;

import com.mh.data.mongo.model.Comment;
import com.mh.data.mongo.model.Post;
import com.mh.data.request.CommentRequest;
import com.mh.data.response.CommentResponse;
import com.mh.tables.pojos.User;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class CommentMapper {

    @Autowired private UserMapper userMapper;
    @Autowired private PostMapper postMapper;

    @Named("toSingleCommentResponse")
    public abstract CommentResponse toCommentResponse(Comment comment);

    @IterableMapping(qualifiedByName = "toSingleCommentResponse")
    public abstract List<CommentResponse> toCommentResponses(List<Comment> commentList);

    public CommentResponse toCommentResponse(Comment comment, Post post, User user) {
        CommentResponse commentResponse = toCommentResponse(comment);
        commentResponse.setPost(postMapper.toPostResponse(post));
        commentResponse.setUser(userMapper.toUserResponse(user));
        return commentResponse;
    }

    public abstract Comment toComment(CommentRequest commentRequest);
}
