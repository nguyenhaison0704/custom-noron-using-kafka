package com.mh.data.mapper;

import com.mh.data.mongo.model.Post;
import com.mh.data.request.PostRequest;
import com.mh.data.response.PostResponse;
import com.mh.tables.pojos.Topic;
import com.mh.tables.pojos.User;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class PostMapper {

    @Autowired private UserMapper userMapper;
    @Autowired private TopicMapper topicMapper;

    @Named("toSinglePostResponse")
    public abstract PostResponse toPostResponse(Post post);

    @IterableMapping(qualifiedByName = "toSinglePostResponse")
    public abstract List<PostResponse> toPostResponses(List<Post> postList);

    public PostResponse toPostResponse(Post post, User user, Topic topic) {
        PostResponse postResponse = toPostResponse(post);
        postResponse.setUser(userMapper.toUserResponse(user));
        postResponse.setTopic(topicMapper.toTopicResponse(topic));
        return postResponse;
    }

    public abstract Post toPost(PostRequest postRequest);
}
