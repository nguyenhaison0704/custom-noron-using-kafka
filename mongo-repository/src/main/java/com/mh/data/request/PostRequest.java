package com.mh.data.request;

public class PostRequest {
    private String id;
    private String topicId;
    private Long numView;
    private Long numComment;
    private String content;
    private String userId;
}
