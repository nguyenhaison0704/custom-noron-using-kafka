package com.mh.data.request;

import lombok.Data;

@Data
public class CommentRequest {
    private String id;
    private String content;
    private String postId;
    private String userId;
}
