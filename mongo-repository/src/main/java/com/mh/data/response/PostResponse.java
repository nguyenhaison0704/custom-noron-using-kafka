package com.mh.data.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PostResponse {
    private String id;
    private TopicResponse topic;
    private Long numView;
    private Long numComment;
    private String content;
    private UserResponse user;
}
