package com.mh.data.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CommentResponse {
    private String id;
    private String content;
    private PostResponse post;
    private UserResponse user;
}
