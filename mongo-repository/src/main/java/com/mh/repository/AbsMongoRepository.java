package com.mh.repository;

import com.mh.repository.util.ConvertBetweenDocumentAndP;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateManyModel;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

import static com.mh.repository.util.ConvertBetweenDocumentAndP.convertDocumentToP;
import static com.mh.repository.util.ConvertBetweenDocumentAndP.convertPToDocument;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;


public abstract class AbsMongoRepository<P> {
    @Autowired
    private MongoDatabase mongoDatabase;

    private Class<P> pClass;

    protected abstract String getCollection();

    protected MongoCollection<Document> collection;

    @PostConstruct
    public void init() {
        pClass = (Class<P>) ((ParameterizedType) getClass()
                .getGenericSuperclass())
                .getActualTypeArguments()[0];
        collection = mongoDatabase.getCollection(this.getCollection());
    }

    public P findById(String id) {
        return convertDocumentToP(Objects.requireNonNull(collection.find(eq("_id", id)).first()), pClass);
    }

    public List<P> findByIds(String idList) {
        List<Document> documents = new ArrayList<>();
        collection.find(in("_id", idList)).into(documents);
        return documents.stream().map(document -> {
            return convertDocumentToP(document, pClass);
        }).collect(Collectors.toList());
    }

    public List<P> findAll() {
        List<Document> documents = new ArrayList<>();
        collection.find().into(documents);
        return documents.stream().map(document -> convertDocumentToP(document, pClass))
                .collect(Collectors.toList());
    }

    public int createOne(P p) {
        collection.insertOne(convertPToDocument(p));
        return 1;
    }

    public int createMany(List<P> pList) {
        if(pList == null) return 0;
        collection.insertMany(pList.stream().map(ConvertBetweenDocumentAndP::convertPToDocument)
                .collect(Collectors.toList()));
        return 1;
    }

    public int update(P p) {
        Document document = convertPToDocument(p);
        List<Bson> bsonList = nullFieldFilter(document);
        collection.updateOne(eq("_id", document.get("_id")), Updates.combine(bsonList));
        return 1;
    }

    private List<Bson> nullFieldFilter(Document document) {
        List<Bson> bsonList = new ArrayList<>();
        document.entrySet().forEach(entry -> {
            if (entry.getValue() != null) {
                bsonList.add(Updates.set(entry.getKey(), entry.getValue()));
            }
        });
        return bsonList;
    }

    public int updateMany(List<P> pList) {
        if(pList == null) return 0;
        List<Document> documentList = ConvertBetweenDocumentAndP.convertPListToDocuments(pList);
        collection.bulkWrite(documentList.parallelStream()
                .map(document -> {
                    return new UpdateManyModel<Document>(eq("_id", document.get("_id")), nullFieldFilter(document));
                })
                .collect(Collectors.toList()));
        return 1;
    }

    public int delete(String id) {
        collection.deleteOne(eq("_id", id));
        return 1;
    }

    public int deleteMany(List<String> idList) {
        if(idList == null) return 0;
        collection.deleteMany(in("_id", idList));
        return 1;
    }
}
