package com.mh.repository.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.bson.Document;

import java.util.List;
import java.util.stream.Collectors;

public class ConvertBetweenDocumentAndP {
    private static final ObjectMapper objectMapper = new ObjectMapper()
                        .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

    public static <P> P convertDocumentToP(Document document, Class<P> pClass) {
        document.put("id", document.get("id"));
        document.remove("_id");
        return objectMapper.convertValue(document, pClass);
    }

    public static <P> Document convertPToDocument(P p) {
        Document document = objectMapper.convertValue(p, Document.class);
        document.put("_id", document.get("id"));
        document.remove("id");
        return document;
    }

    public static <P> List<Document> convertPListToDocuments(List<P> pList) {
        return pList.stream()
                .map(ConvertBetweenDocumentAndP::convertPToDocument)
                .collect(Collectors.toList());
    }
}
