package com.mh.repository.impl;

import com.mh.data.mongo.model.Comment;
import com.mh.repository.AbsMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CommentRepositoryImpl extends AbsMongoRepository<Comment> {
    @Override
    protected String getCollection() {
        return "comment";
    }
}
