package com.mh.repository.impl;

import com.mh.data.mongo.model.Post;
import com.mh.repository.AbsMongoRepository;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.inc;
import static java.util.stream.Collectors.groupingBy;

@Repository
public class PostRepositoryImpl extends AbsMongoRepository<Post> {
    @Override
    protected String getCollection() {
        return "post";
    }

    public int updateNumView(String id, long numView) {
        return (int) collection.updateOne(eq("_id", id), Updates.set("num_view", numView))
                .getModifiedCount();
    }

    public int updateNumCommentSinglePost(String id, long numComment) {
        return (int) collection.updateOne(eq("_id", id), Updates.set("num_comment", numComment))
                .getModifiedCount();
    }

    public int incNumCommentMultiPost(List<String> idList) {
        return increaseFieldValue(idList, "num_comment");
    }

    public int incNumViewMultiPost(List<String> idList) {
        return increaseFieldValue(idList, "num_view");
    }

    private int increaseFieldValue(List<String> idList, String num_view) {
        Map<String, Long> numCommentMap = idList.parallelStream()
                .collect(groupingBy(id -> id, Collectors.counting()));
        collection.bulkWrite(numCommentMap.entrySet().stream()
                .map(entry -> {
                    return new UpdateOneModel<Document>(eq("_id", entry.getKey()),
                            inc(num_view, entry.getValue()));
                })
                .collect(Collectors.toList()));
        return 1;
    }

    public int decNumCommentMultiPost(List<String> idList) {
        decreaseFieldValue(idList, "num_comment");
        return 1;
    }

    public int decNumViewMultiPost(List<String> idList) {
        decreaseFieldValue(idList, "num_view");
        return 1;
    }

    private void decreaseFieldValue(List<String> idList, String num_view) {
        Map<String, Long> numCommentMap = idList.parallelStream()
                .collect(groupingBy(id -> id, Collectors.counting()));
        collection.bulkWrite(numCommentMap.entrySet().stream()
                .map(entry -> {
                    return new UpdateOneModel<Document>(eq("_id", entry.getKey()),
                            inc(num_view, -entry.getValue()));
                })
                .collect(Collectors.toList()));
    }
}
