package com.mh.data.mongo.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Post {
    private String id;
    private String topicId;
    private Long numView;
    private Long numComment;
    private String content;
    private String userId;
}
