package com.mh.data.mongo.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Comment {
    private String id;
    private String content;
    private String postId;
    private String userId;
}
