package com.mh.service;

import com.mh.data.mapper.UserMapper;
import com.mh.data.request.UserRequest;
import com.mh.data.response.UserResponse;
import com.mh.elasticsearch.service.UserESService;
import com.mh.kafka.data.UserMessage;
import com.mh.kafka.producer.impl.UserProducerImpl;
import io.reactivex.rxjava3.core.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired private UserMapper userMapper;
    @Autowired private UserProducerImpl userProducer;
    @Autowired private UserESService userESService;
    public Single<List<UserResponse>> getAllUsers() {
        return Single.create(emitter -> emitter
                .onSuccess(userMapper.toUserResponses(userESService.findAll())));
    }

    public Single<UserResponse> getUserById(String id) {
        return Single.create(emitter -> emitter
                .onSuccess(userMapper.toUserResponse(userESService.findById(id))));
    }

    public Single<Integer> createUser(UserRequest userRequest) {
        return Single.create(emitter -> {
            UserMessage userMessage = new UserMessage();
            userMessage.setT(userMapper.toUser(userRequest));
            userMessage.setMethod("user:add");
            emitter.onSuccess(userProducer.sendMessage(userMessage));
        });
    }

    public Single<Integer> updateUser(UserRequest userRequest) {
        return Single.create(emitter -> {
            UserMessage userMessage = new UserMessage();
            userMessage.setT(userMapper.toUser(userRequest));
            userMessage.setMethod("user:update");
            emitter.onSuccess(userProducer.sendMessage(userMessage));
        });
    }

    public Single<Integer> deleteUser(String id) {
        return Single.create(emitter -> {
            UserMessage userMessage = new UserMessage();
            userMessage.setId(id);
            userMessage.setMethod("user:delete");
            emitter.onSuccess(userProducer.sendMessage(userMessage));
        });
    }
}
