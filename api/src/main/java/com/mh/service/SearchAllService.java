package com.mh.service;

import com.mh.elasticsearch.service.SearchAllESService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SearchAllService {
    @Autowired private SearchAllESService searchAllESService;

    public List<Object> searchById(String id) {
        return searchAllESService.findById(id);
    }

    public List<Object> searchByParam(String param) {
        return searchAllESService.findByParam(param);
    }

    public List<Object> searchByFields(Map<String, String> fieldMap) {
        return searchAllESService.findByFields(fieldMap);
    }
}
