package com.mh.service;

import com.mh.data.mapper.TopicMapper;
import com.mh.data.request.TopicRequest;
import com.mh.data.response.TopicResponse;
import com.mh.elasticsearch.service.TopicESService;
import com.mh.kafka.data.TopicMessage;
import com.mh.kafka.producer.impl.TopicProducerImpl;
import com.mh.repository.impl.TopicRepositoryImpl;
import io.reactivex.rxjava3.core.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicService {
    @Autowired private TopicMapper topicMapper;
    @Autowired private TopicProducerImpl topicProducer;
    @Autowired private TopicESService topicESService;

    public Single<List<TopicResponse>> getAllTopics() {
        return Single.create(emitter -> emitter
                .onSuccess(topicMapper.toTopicResponses(topicESService.findAll())));
    }

    public Single<TopicResponse> getTopicById(String id) {
        return Single.create(emitter -> emitter
                .onSuccess(topicMapper.toTopicResponse(topicESService.findById(id))));
    }

    public Single<Integer> createTopic(TopicRequest topicRequest) {
        return Single.create(emitter -> {
            TopicMessage topicMessage = new TopicMessage();
            topicMessage.setT(topicMapper.toTopic(topicRequest));
            topicMessage.setMethod("topic:add");
            emitter.onSuccess(topicProducer.sendMessage(topicMessage));
        });
    }

    public Single<Integer> updateTopic(TopicRequest topicRequest) {
        return Single.create(emitter -> {
            TopicMessage topicMessage = new TopicMessage();
            topicMessage.setT(topicMapper.toTopic(topicRequest));
            topicMessage.setMethod("topic:update");
            emitter.onSuccess(topicProducer.sendMessage(topicMessage));
        });
    }

    public Single<Integer> deleteTopic(String id) {
        return Single.create(emitter -> {
            TopicMessage topicMessage = new TopicMessage();
            topicMessage.setId(id);
            topicMessage.setMethod("topic:delete");
            emitter.onSuccess(topicProducer.sendMessage(topicMessage));
        });
    }
}
