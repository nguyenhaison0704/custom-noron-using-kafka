package com.mh.service;

import com.mh.data.mapper.PostMapper;
import com.mh.data.request.PostRequest;
import com.mh.data.response.PostResponse;
import com.mh.elasticsearch.service.PostESService;
import com.mh.kafka.data.PostMessage;
import com.mh.kafka.producer.impl.PostProducerImpl;
import io.reactivex.rxjava3.core.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {
    @Autowired private PostMapper postMapper;
    @Autowired private PostProducerImpl postProducer;
    @Autowired private PostESService postESService;

    public Single<List<PostResponse>> getAllPosts() {
        return Single.create(emitter -> emitter
                .onSuccess(postMapper.toPostResponses(postESService.findAll())));
    }

    public Single<PostResponse> getPostById(String id) {
        return Single.create(emitter -> emitter
                .onSuccess(postMapper.toPostResponse(postESService.findById(id))));
    }

    public Single<Integer> createPost(PostRequest postRequest) {
        return Single.create(emitter -> {
            PostMessage postMessage = new PostMessage();
            postMessage.setT(postMapper.toPost(postRequest));
            postMessage.setMethod("post:add");
            emitter.onSuccess(postProducer.sendMessage(postMessage));
        });
    }

    public Single<Integer> updatePost(PostRequest postRequest) {
        return Single.create(emitter -> {
            PostMessage postMessage = new PostMessage();
            postMessage.setT(postMapper.toPost(postRequest));
            postMessage.setMethod("post:update");
            emitter.onSuccess(postProducer.sendMessage(postMessage));
        });
    }

    public Single<Integer> deletePost(String id) {
        return Single.create(emitter -> {
            PostMessage postMessage = new PostMessage();
            postMessage.setId(id);
            postMessage.setMethod("post:delete");
            emitter.onSuccess(postProducer.sendMessage(postMessage));
        });
    }

    public Single<Integer> increaseView(String id) {
        return Single.create(emitter -> {
            PostMessage postMessage = new PostMessage();
            postMessage.setId(id);
            postMessage.setMethod("post:inc-comment");
            emitter.onSuccess(postProducer.sendMessage(postMessage));
        });
    }
}
