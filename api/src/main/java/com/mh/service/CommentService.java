package com.mh.service;

import com.mh.data.mapper.CommentMapper;
import com.mh.data.mongo.model.Comment;
import com.mh.data.request.CommentRequest;
import com.mh.data.response.CommentResponse;
import com.mh.elasticsearch.service.CommentESService;
import com.mh.kafka.data.CommentMessage;
import com.mh.kafka.data.PostMessage;
import com.mh.kafka.producer.impl.CommentProducerImpl;
import com.mh.kafka.producer.impl.PostProducerImpl;
import io.reactivex.rxjava3.core.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    @Autowired private CommentMapper commentMapper;
    @Autowired private CommentProducerImpl commentProducer;

    @Autowired private PostProducerImpl postProducer;

    @Autowired private CommentESService commentESService;

    public Single<List<CommentResponse>> getAllComments() {
        return Single.create(emitter -> emitter
                .onSuccess(commentMapper.toCommentResponses(commentESService.findAll())));
    }

    public Single<CommentResponse> getCommentById(String id) {
        return Single.create(emitter -> emitter
                .onSuccess(commentMapper.toCommentResponse(commentESService.findById(id))));
    }

    public Single<Integer> createComment(CommentRequest commentRequest) {
        Single<Integer> commentSingle = Single.create(emitter -> {
            CommentMessage commentMessage = new CommentMessage();
            commentMessage.setT(commentMapper.toComment(commentRequest));
            commentMessage.setMethod("comment:add");
            emitter.onSuccess(commentProducer.sendMessage(commentMessage));
        });

        Single<Integer> postSingle = Single.create(emitter -> {
            PostMessage postMessage = new PostMessage();
            postMessage.setId(commentRequest.getPostId());
            postMessage.setMethod("post:inc-comment");
            emitter.onSuccess(postProducer.sendMessage(postMessage));
        });

        return Single.zip(commentSingle, postSingle, (commentResult, postResult) -> {
            return 1;
        });

    }

    public Single<Integer> updateComment(CommentRequest commentRequest) {
        return Single.create(emitter -> {
            CommentMessage commentMessage = new CommentMessage();
            commentMessage.setT(commentMapper.toComment(commentRequest));
            commentMessage.setMethod("comment:update");
            emitter.onSuccess(commentProducer.sendMessage(commentMessage));
        });
    }

    public Single<Integer> deleteComment(String id) {
        Single<Comment> comment = Single.create(emitter -> {
            emitter.onSuccess(commentESService.findById(id));
        });

        Single<Integer> commentSingle = Single.create(emitter -> {
            CommentMessage commentMessage = new CommentMessage();
            commentMessage.setId(id);
            commentMessage.setMethod("comment:delete");
            emitter.onSuccess(commentProducer.sendMessage(commentMessage));
        });


        return Single.zip(comment, commentSingle, (commentResult, commentSingleResult) -> {
            PostMessage postMessage = new PostMessage();
            postMessage.setId(commentResult.getPostId());
            postMessage.setMethod("post:inc-comment");
            return postProducer.sendMessage(postMessage);
        });
    }
}
