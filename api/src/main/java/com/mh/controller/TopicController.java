package com.mh.controller;

import com.mh.data.request.TopicRequest;
import com.mh.data.response.TopicResponse;
import com.mh.service.TopicService;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TopicController {
    @Autowired
    private TopicService topicService;

    @GetMapping("/api/topic")
    public Single<ResponseEntity<List<TopicResponse>>> getListTopic() {
        return topicService.getAllTopics().observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @GetMapping("/api/topic/{id}")
    public Single<ResponseEntity<TopicResponse>> getTopicById(@RequestPart String id) {
        return topicService.getTopicById(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PostMapping("/api/topic")
    public Single<ResponseEntity<Integer>> createTopic(@RequestBody TopicRequest topicRequest) {
        return topicService.createTopic(topicRequest).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PutMapping("/api/topic")
    public Single<ResponseEntity<Integer>> updateTopic(@RequestBody TopicRequest topicRequest) {
        return topicService.updateTopic(topicRequest).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @DeleteMapping("/api/topic/{id}")
    public Single<ResponseEntity<Integer>> deleteTopic(@PathVariable String id) {
        return topicService.deleteTopic(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }
}
