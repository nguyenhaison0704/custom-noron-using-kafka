package com.mh.controller;

import com.mh.data.request.CommentRequest;
import com.mh.data.response.CommentResponse;
import com.mh.service.CommentService;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/api/comment")
    public Single<ResponseEntity<List<CommentResponse>>> getListComment() {
        return commentService.getAllComments().observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @GetMapping("/api/comment/{id}")
    public Single<ResponseEntity<CommentResponse>> getCommentById(@RequestPart String id) {
        return commentService.getCommentById(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PostMapping("/api/comment")
    public Single<ResponseEntity<Integer>> createComment(@RequestBody CommentRequest commentRequest) {
        return commentService.createComment(commentRequest).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PutMapping("/api/comment")
    public Single<ResponseEntity<Integer>> updateComment(@RequestBody CommentRequest commentRequest) {
        return commentService.updateComment(commentRequest).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @DeleteMapping("/api/comment/{id}")
    public Single<ResponseEntity<Integer>> deleteComment(@PathVariable String id) {
        return commentService.deleteComment(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }
}
