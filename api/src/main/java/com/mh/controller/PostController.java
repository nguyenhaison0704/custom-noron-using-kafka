package com.mh.controller;

import com.mh.data.request.PostRequest;
import com.mh.data.response.PostResponse;
import com.mh.service.PostService;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PostController {

    @Autowired private PostService postService;

    @GetMapping("/api/post")
    public Single<ResponseEntity<List<PostResponse>>> getListPost() {
        return postService.getAllPosts().observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @GetMapping("/api/post/{id}")
    public Single<ResponseEntity<PostResponse>> getPostById(@RequestPart String id) {
        return postService.getPostById(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PostMapping("/api/post")
    public Single<ResponseEntity<Integer>> createPost(@RequestBody PostRequest postRequest) {
        return postService.createPost(postRequest).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PutMapping("/api/post")
    public Single<ResponseEntity<Integer>> updatePost(@RequestBody PostRequest postRequest) {
        return postService.updatePost(postRequest).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PutMapping("api/post/incView/{id}")
    public Single<ResponseEntity<Integer>> increaseViewNum(@PathVariable String id) {
        return postService.increaseView(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @DeleteMapping("/api/post/{id}")
    public Single<ResponseEntity<Integer>> deletePost(@PathVariable String id) {
        return postService.deletePost(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }
}
