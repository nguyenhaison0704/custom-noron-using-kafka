package com.mh.controller;

import com.mh.service.SearchAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class SearchAllController {

    @Autowired private SearchAllService searchAllService;

    @GetMapping("api/_search")
    public ResponseEntity<List<Object>> searchByParam(@RequestParam Map<String, String> fieldMap) {
        String param = fieldMap.getOrDefault("param", null);
        if(param != null) return ResponseEntity.ok(searchAllService.searchByParam(param));
        else {
            return ResponseEntity.ok(searchAllService.searchByFields(fieldMap));
        }
    }
}
