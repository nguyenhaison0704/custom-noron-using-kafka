package com.mh.controller;

import com.mh.data.request.UserRequest;
import com.mh.data.response.UserResponse;
import com.mh.service.UserService;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/api/user")
    public Single<ResponseEntity<List<UserResponse>>> getListUser() {
        return userService.getAllUsers().observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @GetMapping("/api/user/{id}")
    public Single<ResponseEntity<UserResponse>> getUserById(@PathVariable String id) {
        return userService.getUserById(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PostMapping("/api/user")
    public Single<ResponseEntity<Integer>> createUser(@RequestBody UserRequest userRequest) {
        return userService.createUser(userRequest).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @PutMapping("/api/user")
    public Single<ResponseEntity<Integer>> updateUser(@RequestBody UserRequest userRequest) {
        return userService.updateUser(userRequest).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @DeleteMapping("/api/user/{id}")
    public Single<ResponseEntity<Integer>> deleteUser(@PathVariable String id) {
        return userService.deleteUser(id).observeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }
}
