package com.mh.elasticsearch.config;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class APIClient {

    private String cloudServer = "custom-noron.es.asia-southeast1.gcp.elastic-cloud.com";
    private int cloudPort = 9243;
    private String localServer = "localhost";
    private int localPort = 9200;

    private String username = "elastic";
    private String cloudPassword = "adrXmdUahyIEbqVWZu6edXDQ";
    private String localpassword = "EEzvPyUI-i6+HiFTQQY1";

    private String httpCACertificate = "a01a235cce79de5c0fcfde96694dfd57187ea9aeca3ba715c17a84d115deda09";

    @Bean
    public ElasticsearchClient client() {

        ElasticsearchTransport transport = transport();

        // And create the API client
        return new ElasticsearchClient(transport);
    }

    @Bean
    public ElasticsearchTransport transport() {
        // Create the transport with a Jackson mapper
        return new RestClientTransport(restClient(), new JacksonJsonpMapper());
    }

    private CredentialsProvider getCredentialsProvider() {
        final CredentialsProvider credentialsProvider =
                new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, cloudPassword));
        return credentialsProvider;
    }

    @Bean
    public RestClient restClient() {
        // Create the low-level client
        return RestClient.builder(
                        new HttpHost(localServer, localPort))
                .setHttpClientConfigCallback(httpAsyncClientBuilder ->
                        httpAsyncClientBuilder.setDefaultCredentialsProvider(getCredentialsProvider()))
                .build();
    }
}
