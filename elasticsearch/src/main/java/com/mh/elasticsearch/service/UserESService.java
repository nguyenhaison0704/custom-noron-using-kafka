package com.mh.elasticsearch.service;

import com.mh.tables.pojos.User;
import org.springframework.stereotype.Component;

@Component
public class UserESService extends AbsESService<User> {
    @Override
    protected String getIndex() {
        return "user";
    }

    @Override
    protected String getId(User user) {
        return user.getId();
    }

//    @Autowired private ElasticsearchClient client;
//
//    @SneakyThrows
//    public List<User> findAll() {
//        MatchAllQuery matchAllQuery = QueryBuilders.matchAll().build();
//        SearchResponse<User> search = client.search(r -> r
//                .index("user")
//                        .query(q -> q.matchAll(matchAllQuery)),
//                User.class);
//
//        return executeSearchQuery(search);
//    }
//
//    private List<User> executeSearchQuery(SearchResponse<User> search) {
//        return search.hits().hits()
//                .stream()
//                .map(userHit -> {
//                    assert userHit.source() != null;
//                    return userHit.source();
//                })
//                .collect(Collectors.toList());
//    }
//
//    @SneakyThrows
//    public List<User> findByIds(List<String> idList) {
//        IdsQuery idsQuery = QueryBuilders.ids()
//                .values(idList)
//                .build();
//        SearchResponse<User> search = client.search(r -> r
//                .index("user")
//                        .query(q -> q.ids(idsQuery)),
//                User.class);
//        return executeSearchQuery(search);
//    }
//
//    @SneakyThrows
//    public User findById(String id) {
//        IdsQuery idsQuery = QueryBuilders.ids()
//                .values(id)
//                .build();
//        SearchResponse<User> search = client.search(r -> r
//                        .index("user")
//                        .query(q -> q.ids(idsQuery)),
//                User.class);
//        return executeSearchQuery(search).get(0);
//    }
//
//    @SneakyThrows
//    public Integer createOne(User user) {
//        CreateRequest<User> request = new CreateRequest.Builder<User>()
//                .index("user")
//                .document(user)
//                .id(user.getId())
//                .build();
//        CreateResponse response = client.create(request);
//        return 1;
//    }
//
//    @SneakyThrows
//    public Integer createMany(List<User> users) {
//        List<BulkOperation> bulkOperations = users.stream().map(user -> {
//            CreateOperation<User> createOperation = new CreateOperation.Builder<User>()
//                    .document(user)
//                    .id(user.getId())
//                    .build();
//            return new BulkOperation.Builder().create(createOperation).build();
//        }).collect(Collectors.toList());
//        BulkRequest bulkRequest = new BulkRequest.Builder()
//                .index("user")
//                .operations(bulkOperations)
//                .build();
//        BulkResponse result = client.bulk(bulkRequest);
//        return 1;
//    }
//
//    @SneakyThrows
//    public Integer updateOne(User user) {
//        UpdateRequest<User, User> request = new UpdateRequest.Builder<User, User>()
//                .docAsUpsert(true)
//                .index("user")
//                .doc(user)
//                .id(user.getId())
//                .build();
//
//        UpdateResponse<User> updateResponse = client.update(request, User.class);
//        return 1;
//    }
//
//    @SneakyThrows
//    public Integer updateMany(List<User> users) {
//        List<BulkOperation> bulkOperations = users.stream().map(user -> {
//
//            List<Field> declaredFields = List.of(user.getClass().getDeclaredFields());
//            Map<String, Object> fieldMap = declaredFields.stream()
//                    .filter(field -> {
//                        try {
//                            field.setAccessible(true);
//                            return (!(field.get(user) == null))&&(!field.getName().equals("serialVersionUID"));
//                        } catch (IllegalAccessException e) {
//                            throw new RuntimeException(e);
//                        }
//                    })
//                    .collect(Collectors.toMap(field -> field.getName(), field -> {
//                        try {
//                            return field.get(user);
//                        } catch (IllegalAccessException e) {
//                            return null;
//                        }
//                    }));
//
//            UpdateAction<User, Map<String, Object>> updateAction = new UpdateAction.Builder<User, Map<String, Object>>()
//                    .doc(fieldMap)
//                    .docAsUpsert(true)
//                    .build();
//
//            UpdateOperation<User, Map<String, Object>> updateOperation = new UpdateOperation.Builder<User, Map<String, Object>>()
//                    .id(user.getId())
//                    .index("user")
//                    .action(updateAction)
//                    .build();
//
//            return new BulkOperation.Builder().update(updateOperation).build();
//        }).collect(Collectors.toList());
//
//        BulkRequest bulkRequest = new BulkRequest.Builder()
//                .index("user")
//                .operations(bulkOperations)
//                .build();
//
//        BulkResponse bulkResponse = client.bulk(bulkRequest);
//        return 1;
//    }
//
//    @SneakyThrows
//    public Integer deleteOne(String id) {
//        DeleteRequest deleteRequest = new DeleteRequest.Builder()
//                .id(id)
//                .index("user")
//                .build();
//        DeleteResponse deleteResponse = client.delete(deleteRequest);
//        return 1;
//    }
//
//    @SneakyThrows
//    public Integer deleteMany(List<String> ids) {
//        List<BulkOperation> operations = ids.stream().map(id -> {
//            DeleteOperation deleteOperation = new DeleteOperation.Builder()
//                    .id(id)
//                    .build();
//            return new BulkOperation.Builder().delete(deleteOperation).build();
//        }).collect(Collectors.toList());
//
//        BulkRequest bulkRequest = new BulkRequest.Builder()
//                .operations(operations)
//                .index("user")
//                .build();
//
//        BulkResponse bulkResponse = client.bulk(bulkRequest);
//        return 1;
//    }
}