package com.mh.elasticsearch.service;

import com.mh.data.mongo.model.Comment;
import org.springframework.stereotype.Component;

@Component
public class CommentESService extends AbsESService<Comment> {
    @Override
    protected String getIndex() {
        return "comment";
    }

    @Override
    protected String getId(Comment comment) {
        return comment.getId();
    }
}
