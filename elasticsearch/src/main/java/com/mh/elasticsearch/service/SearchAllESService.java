package com.mh.elasticsearch.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.*;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component
public class SearchAllESService {

    @Autowired
    private ElasticsearchClient client;

    @SneakyThrows
    public List<Object> findAll() {
        MatchAllQuery matchAllQuery = QueryBuilders.matchAll().build();
        SearchResponse<Object> search = client.search(r -> r
                        .query(q -> q.matchAll(matchAllQuery)),
                Object.class);
        return executeSearchQuery(search);
    }

    private List<Object> executeSearchQuery(SearchResponse<Object> search) {
        return search.hits().hits()
                .stream()
                .map(Hit::source)
                .collect(toList());
    }

    @SneakyThrows
    public List<Object> findById(String id) {
        IdsQuery idsQuery = QueryBuilders.ids()
                .values(id)
                .build();
        SearchResponse<Object> search = client.search(r -> r
                        .query(q -> q.ids(idsQuery)),
                Object.class);
        return executeSearchQuery(search);
    }

    @SneakyThrows
    public List<Object> findByParam(String param) {
        param = "*" + param + "*";
        QueryStringQuery query = QueryBuilders.queryString()
                .query(param)
                .build();
        SearchRequest searchRequest = new SearchRequest.Builder()
                .query(q -> q.queryString(query))
                .build();
        SearchResponse<Object> searchResponse = client.search(searchRequest, Object.class);

        return executeSearchQuery(searchResponse);
    }

    @SneakyThrows
    public List<Object> findByFields(Map<String, String> fieldMap) {

        Set<String> fields = fieldMap.keySet();

        List<Query> queries = fields.stream()
                .map(field -> new Query.Builder().match(QueryBuilders.match()
                                .field(field)
                                .query("*" + fieldMap.get(field) + "*")
                                .build())
                        .build())
                .collect(Collectors.toList());

        BoolQuery query = QueryBuilders.bool()
                .should(queries)
                .build();

        SearchRequest request = new SearchRequest.Builder()
                .query(q -> q.bool(query))
                .build();

        SearchResponse<Object> response = client.search(request, Object.class);

        return executeSearchQuery(response);
    }
}
