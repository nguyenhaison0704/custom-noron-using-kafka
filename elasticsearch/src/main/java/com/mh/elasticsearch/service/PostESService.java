package com.mh.elasticsearch.service;

import com.mh.data.mongo.model.Post;
import org.springframework.stereotype.Component;

@Component
public class PostESService extends AbsESService<Post> {
    @Override
    protected String getIndex() {
        return "post";
    }

    @Override
    protected String getId(Post post) {
        return post.getId();
    }
}
