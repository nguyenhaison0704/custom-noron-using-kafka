package com.mh.elasticsearch.service;

import com.mh.tables.pojos.Topic;
import org.springframework.stereotype.Component;

@Component
public class TopicESService extends AbsESService<Topic> {
    @Override
    protected String getIndex() {
        return "topic";
    }

    @Override
    protected String getId(Topic topic) {
        return topic.getId();
    }
}
