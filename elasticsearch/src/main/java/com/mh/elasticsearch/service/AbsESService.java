package com.mh.elasticsearch.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.IdsQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchAllQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryStringQuery;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.bulk.*;
import co.elastic.clients.elasticsearch.core.search.Hit;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbsESService<P> {
    @Autowired private ElasticsearchClient client;

    Class<P> pClass;

    protected abstract String getIndex();
    protected abstract String getId(P p);

    @PostConstruct
    @SneakyThrows
    public void init() {
        pClass = (Class<P>) ((ParameterizedType)this.getClass()
                .getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    @SneakyThrows
    public List<P> findAll() {
        MatchAllQuery matchAllQuery = QueryBuilders.matchAll().build();
        SearchResponse<P> search = client.search(r -> r
                .index(this.getIndex())
                .query(q -> q.matchAll(matchAllQuery)),
                pClass);
        return executeSearchQuery(search);
    }

    private List<P> executeSearchQuery(SearchResponse<P> search) {
        return search.hits().hits()
                .stream()
                .map(Hit::source)
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<P> findByIds(List<String> idList) {
        IdsQuery idsQuery = QueryBuilders.ids()
                .values(idList)
                .build();
        SearchResponse<P> search = client.search(r -> r
                        .index(this.getIndex())
                        .query(q -> q.ids(idsQuery)),
                pClass);
        return executeSearchQuery(search);
    }

    @SneakyThrows
    public P findById(String id) {
        IdsQuery idsQuery = QueryBuilders.ids()
                .values(id)
                .build();
        SearchResponse<P> search = client.search(r -> r
                        .index(this.getIndex())
                        .query(q -> q.ids(idsQuery)),
                pClass);
        return executeSearchQuery(search).get(0);
    }

    @SneakyThrows
    public List<P> findByParam(String param) {
        QueryStringQuery query = QueryBuilders.queryString()
                .query(param)
                .build();
        SearchRequest searchRequest = new SearchRequest.Builder()
                .index(this.getIndex())
                .query(q -> q.queryString(query))
                .build();
        SearchResponse<P> searchResponse = client.search(searchRequest, pClass);

        return executeSearchQuery(searchResponse);
    }

    @SneakyThrows
    public Integer createOne(P p) {
        CreateRequest<P> request = new CreateRequest.Builder<P>()
                .index(this.getIndex())
                .document(p)
                .id(this.getId(p))
                .build();
        CreateResponse response = client.create(request);
        return 1;
    }

    @SneakyThrows
    public Integer createMany(List<P> pList) {
        List<BulkOperation> bulkOperations = pList.stream().map(p -> {
            CreateOperation<P> createOperation = new CreateOperation.Builder<P>()
                    .document(p)
                    .id(this.getId(p))
                    .build();
            return new BulkOperation.Builder().create(createOperation).build();
        }).collect(Collectors.toList());
        execBulkRequest(bulkOperations);
        return 1;
    }

    private void execBulkRequest(List<BulkOperation> bulkOperations) throws IOException {
        BulkRequest bulkRequest = new BulkRequest.Builder()
                .index(this.getIndex())
                .operations(bulkOperations)
                .build();
        BulkResponse result = client.bulk(bulkRequest);
    }

    @SneakyThrows
    public Integer updateOne(P p) {
        UpdateRequest<P, P> request = new UpdateRequest.Builder<P, P>()
                .docAsUpsert(true)
                .index(this.getIndex())
                .doc(p)
                .id(this.getId(p))
                .build();

        UpdateResponse<P> updateResponse = client.update(request, pClass);
        return 1;
    }

    @SneakyThrows
    public Integer updateMany(List<P> pList) {
        List<BulkOperation> bulkOperations = pList.stream().map(p -> {

            Map<String, Object> fieldMap = extractNonNullFields(p);

            UpdateAction<P, Map<String, Object>> updateAction = initUpdateAction(fieldMap);

            UpdateOperation<P, Map<String, Object>> updateOperation = new UpdateOperation.Builder<P, Map<String, Object>>()
                    .id(this.getId(p))
                    .index(this.getIndex())
                    .action(updateAction)
                    .build();

            return new BulkOperation.Builder().update(updateOperation).build();
        }).collect(Collectors.toList());

        execBulkRequest(bulkOperations);
        return 1;
    }

    private UpdateAction<P, Map<String, Object>> initUpdateAction(Map<String, Object> fieldMap) {
        UpdateAction<P, Map<String, Object>> updateAction = new UpdateAction.Builder<P, Map<String, Object>>()
                .doc(fieldMap)
                .docAsUpsert(true)
                .build();
        return updateAction;
    }

    private Map<String, Object> extractNonNullFields(P p) {
        List<Field> declaredFields = List.of(p.getClass().getDeclaredFields());
        Map<String, Object> fieldMap = declaredFields.stream()
                .filter(field -> {
                    try {
                        field.setAccessible(true);
                        return (!(field.get(p) == null))&&(!field.getName().equals("serialVersionUID"));
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toMap(field -> field.getName(), field -> {
                    try {
                        return field.get(p);
                    } catch (IllegalAccessException e) {
                        return null;
                    }
                }));
        return fieldMap;
    }

    @SneakyThrows
    public Integer deleteOne(String id) {
        DeleteRequest deleteRequest = new DeleteRequest.Builder()
                .id(id)
                .index(this.getIndex())
                .build();
        DeleteResponse deleteResponse = client.delete(deleteRequest);
        return 1;
    }

    @SneakyThrows
    public Integer deleteMany(List<String> ids) {
        List<BulkOperation> operations = ids.stream().map(id -> {
            DeleteOperation deleteOperation = new DeleteOperation.Builder()
                    .id(id)
                    .build();
            return new BulkOperation.Builder().delete(deleteOperation).build();
        }).collect(Collectors.toList());

        BulkRequest bulkRequest = new BulkRequest.Builder()
                .operations(operations)
                .index(this.getIndex())
                .build();

        BulkResponse bulkResponse = client.bulk(bulkRequest);
        return 1;
    }
}
